import time
import copy
import numpy as np
from multiprocessing import Process, Manager
import functools

 

def how_many_within_range_sequential(row, minimum, maximum):   

    count = 0

    for n in row:

        if minimum <= n <= maximum:

            count += 1

    return count

 

# Complete the how_many_within_range_parallel function below.

def how_many_within_range_parallel(row, minimum, maximum, resultado):
    #implement you solution
    for row in arr:
        count=0
        for n in row:
            if minimum <= n <= maximum:
                count +=1
        resultado.append(count)

 

if __name__ == '__main__':

    # Prepare data
    np.random.RandomState(100)

    arr = np.random.randint(0, 10, size=[4000000, 10])
    ar = arr.tolist()

   
    inicioSec = time.time()
    resultsSec = []
    for row in ar:
        resultsSec.append(how_many_within_range_sequential(row, minimum=4, maximum=8))
    finSec =  time.time()
   
    # You can modify this to adapt to your code
    numeroP = 4
    tam = np.size(arr,0)
    paso = 0

    inicioPar = time.time()   

    with Manager() as manager:
        resultado = manager.list()
        procesos = [] 
        for p in range(numeroP):
            
            proceso = Process(target=how_many_within_range_parallel, args=(arr[paso:paso+tam], 4, 8, resultado))
            proceso.start()
            procesos.append(proceso)
            proceso.join()
        finPar = time.time()
        resultsPar = copy.deepcopy(resultado)
    
    

   

    print('Results are correct!\n' if functools.reduce(lambda x, y : x and y, map(lambda p, q: p == q,resultsSec,resultsPar), True) else 'Results are incorrect!\n')

    print('Secuencial Tiempo %.3f ms \n' % ((finSec - inicioSec)*1000))

    print('Multiprocessing Tiempo %.3f ms \n' % ((finPar - inicioPar)*1000))